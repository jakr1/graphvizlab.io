---
defaults: []
flags:
- write
minimums: []
name: lp
types:
- point
used_by: EGC
---
Label position, [in points](#points).

The position indicates the center of the label.
