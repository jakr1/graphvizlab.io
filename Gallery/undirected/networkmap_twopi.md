---
redirect_from:
  - /_pages/Gallery/undirected/networkmap_twopi.html
layout: gallery
title: Radial Layout of a Network Graph
svg: networkmap_twopi.svg
copyright: Copyright &#169; 1996 AT&amp;T.  All rights reserved.
gv_file: networkmap_twopi.gv.txt
img_src: networkmap_twopi.png
---
This map is a visual representation of the network discovery done by
a recon server, which sweeps the network, and identify hosts in a network
and the full chain of hosts in the route to discovered networks.
