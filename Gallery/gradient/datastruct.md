---
copyright: Copyright &#169; 1996-2004 AT&amp;T.  All rights reserved.
redirect_from:
  - /_pages/Gallery/gradient/datastruct.html
layout: gallery
title: Gradients Applied to Data Struct Example
svg: datastruct.svg
gv_file: datastruct.gv.txt
img_src: datastruct.png
---
Demonstrates an application of gradients to record nodes.
