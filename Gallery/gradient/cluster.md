---
copyright: Copyright &#169; 1996-2004 AT&amp;T.  All rights reserved.
redirect_from:
  - /_pages/Gallery/gradient/cluster.html
layout: gallery
title: Cluster Gradients
svg: cluster.svg
gv_file: cluster.gv.txt
img_src: cluster.png
---
Demonstrates the use of cluster gradients.
